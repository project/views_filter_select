INTRODUCTION
------------

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/views_filter_select

REQUIREMENTS
------------

This module requires the following modules:
* Views

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

* Use it in your code where necessary.


MAINTAINERS
-----------

Current maintainers:
* Michael Schuddings - https://www.drupal.org/u/mschudders
